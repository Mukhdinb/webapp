package com.RasAlGhul.WebApp.BootStrap;

import com.RasAlGhul.WebApp.Repository.CartRepository;
import com.RasAlGhul.WebApp.Repository.ProductRepository;
import com.RasAlGhul.WebApp.model.Cart;
import com.RasAlGhul.WebApp.model.Product;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;


@Component
public class BootStrapData implements CommandLineRunner {

    private final ProductRepository productRepository;
    private final CartRepository cartRepository;

    public BootStrapData(ProductRepository productRepository, CartRepository cartRepository) {
        this.productRepository = productRepository;
        this.cartRepository = cartRepository;
    }

    @Override
    public void run(String... args) throws Exception {

        Product item1 = new Product(1,"12345","this is item1",19.99);
        Product item2 = new Product(2,"23456","this is item2",39.99);
        Product item3 = new Product(1,"34567","this is item3",29.99);
        item2.setAvailible(20);
        item2.setDiscount(10);
        item2.setSize("20x19cm");
        productRepository.save(item1);
        productRepository.save(item2);
        Cart cart1= new Cart();

        cart1.getBroughtProducts().add(item1);
        cart1.getBroughtProducts().add(item2);

        cartRepository.save(cart1);
        cart1.getBroughtProducts().add(item3);
        productRepository.save(item3);

        System.out.println("Starting bootStrap");
        System.out.println("products in cart repo "+ productRepository.count());
        System.out.println("carts "+ cartRepository.count());
    }
}
