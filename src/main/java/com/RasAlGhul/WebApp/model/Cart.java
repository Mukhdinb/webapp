package com.RasAlGhul.WebApp.model;


import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "CART")
public class Cart {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @OneToMany(mappedBy = "cart")
    private Set<Product> broughtProducts;

    public Cart() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Set<Product> getBroughtProducts() {
        return broughtProducts;
    }

    public void setBroughtProducts(Set<Product> broughtProducts) {
        this.broughtProducts = broughtProducts;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Cart cart = (Cart) o;

        return id == cart.id;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }

    @Override
    public String toString() {
        return "Cart{" +
                "id=" + id +
                ", broughtProducts=" + broughtProducts +
                '}';
    }
}
