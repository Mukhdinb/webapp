package com.RasAlGhul.WebApp.model;


import javax.persistence.*;

@Entity
@Table(name = "PRODUCT")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private int category;
    private int availible;
    private String serialNumber;
    private String description;
    private String size;
    private double weight;
    private double price;
    private double discount;

    @ManyToOne
    @JoinColumn(name = "cart_id", nullable = true)
    private Cart cart;


    //constructor


    public Product( int category, String serialNumber, String description, double price) {

        this.category = category;
        this.serialNumber = serialNumber;
        this.description = description;
        this.price = price;
    }

    // \/  getters and setters
    public double getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {

        this.discount = discount;
        price=price * (1-(discount/100));
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    public void setAvailible(int availible) {
        this.availible = availible;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public long getId() {
        return id;
    }

    public int getCategory() {
        return category;
    }

    public int getAvailible() {
        return availible;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public String getDescription() {
        return description;
    }

    public String getSize() {
        return size;
    }

    public double getWeight() {
        return weight;
    }

    public double getPrice() {
        return price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Product product = (Product) o;

        return id == product.id;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", category=" + category +
                ", availible=" + availible +
                ", serialNumber='" + serialNumber + '\'' +
                ", description='" + description + '\'' +
                ", size='" + size + '\'' +
                ", weight=" + weight +
                ", price=" + price +
                ", discount=" + discount +
                ", cart=" + cart +
                '}';
    }
}
