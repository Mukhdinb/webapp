package com.RasAlGhul.WebApp.Repository;

import com.RasAlGhul.WebApp.model.Cart;
import org.springframework.data.repository.CrudRepository;

public interface CartRepository extends CrudRepository<Cart, Long> {
}
