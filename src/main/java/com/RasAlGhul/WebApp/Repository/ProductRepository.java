package com.RasAlGhul.WebApp.Repository;

import com.RasAlGhul.WebApp.model.Product;
import org.springframework.data.repository.CrudRepository;

public interface ProductRepository extends CrudRepository<Product, Long> {
}
