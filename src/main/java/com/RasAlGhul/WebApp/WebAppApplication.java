package com.RasAlGhul.WebApp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebAppApplication {

	public static void main(String[] args) {
		System.out.println("Main");
		SpringApplication.run(WebAppApplication.class, args);
	}

}
